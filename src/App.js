/** @format */

import React, { useState } from 'react';
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  const [errMsg, setErrMsg] = useState('');

  return (
    <div data-test='component-render'>
      <h2 data-test='counter-display'>
        Counter has incremented by <span data-test='count'>{count}</span>
      </h2>
      {errMsg && <span data-test='error-message' style={{ color: 'red' }}>{errMsg}</span>}
      <button
        data-test='increment-button'
        onClick={() => {
          setCount(count + 1); 
          setErrMsg('');
        }}>
        Increment
      </button>
      <button
        data-test='decrement-button'
        onClick={() => {
          count <= 0 ? setErrMsg("Counter can't be 0") : setCount(count - 1);
        }}>
        Decrement
      </button>
      <button data-test='clear-button' onClick={() => setCount(0)}>
        Clear
      </button>
    </div>
  );
}

export default App;
