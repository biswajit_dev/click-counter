/** @format */

import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import App from './App';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const initialSetup = (val) => {
  const wrapper = shallow(<App />);
  return wrapper.find(`[data-test='${val}']`);
};

test('render without error', () => {
  const appCom = initialSetup('component-render');
  expect(appCom.length).toBe(1);
});

test('render inclement button', () => {
  const button = initialSetup('increment-button');
  expect(button.length).toBe(1);
});

test('reder counter display', () => {
  const counterDisplay = initialSetup('counter-display');
  expect(counterDisplay.length).toBe(1);
});

test('render counter display starts with 0', () => {
  const count = initialSetup('count').text();
  expect(count).toBe('0');
});

test('clicking button increment counter display', () => {
  const wrapper = shallow(<App />); 
  // find button and click
  const button = wrapper.find("[data-test='increment-button']");
  button.simulate('click');

  // check the counter
  const count = wrapper.find("[data-test='count']").text();
  expect(count).toBe('1');

  const errMsg = wrapper.find("[data-test='error-message']");
  expect(errMsg.length).toBe(0);
});

test('clear button test', ()=> {
  const wrapper = shallow(<App />);
  const clearButton = wrapper.find("[data-test='clear-button']");
  clearButton.simulate('click');
  const count = wrapper.find("[data-test='count']").text();
  expect(count).toBe("0");
});

test('render declement button', () => {
  const button = initialSetup('decrement-button');
  expect(button.length).toBe(1);
});

test('clicking button decrement error display', () => {
  const wrapper = shallow(<App />); 
  // find button and click
  const button = wrapper.find("[data-test='decrement-button']");
  button.simulate('click');

  const errMsg = wrapper.find("[data-test='error-message']");
  expect(errMsg.length).toBe(1);
});